<!-- figure out how to center header without centering unordered list (bullets) -->
####Healthcare data rights in the 21st century
&nbsp;&nbsp;*HIPAA, HL7, FHIR APIs, and more*
##### HIPAA
* HIPAA, passed in 1996, gives you the right to your healthcare data
* [45 CFR 164.524(c)(2)(i)](https://www.law.cornell.edu/cfr/text/45/164.524) states "covered entity must provide the individual with access to the protected health information in the form and format requested by the individual"
* Compliance is lacking; [Assessment of US Hospital Compliance With Regulations for Patients’ Requests for Medical Records](https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2705850) 
* Complain to [OCR HHS](https://ocrportal.hhs.gov/ocr/smartscreen/main.jsf)
* State laws also apply
* Webinar tom (2019-07-17) on details https://twitter.com/HealthPrivacy/status/1150892591027752961

???
Difference between whining and complaining: complaints are made to the right person.

---
#### Interoperability
* APIs and interoperability improve patient experience
* Long and painful approach with federal incentives (HITECH Act)
* Federal 21st Century Cures Act in 2016 prohibits information blocking
  * See 42 USC § 300jj-52(a)(1)(A) (2016)
* Details left to regulation
  * regulator is The Office of the National Coordinator for Health Information Technology (ONC)
  * report blocking https://www.healthit.gov/form/healthit-feedback-form
  * Regulations near finalization https://www.regulations.gov/docket?D=HHS-ONC-2019-0002

---
#### Nongovernmental ecosystem: HL7 and FHIR
* Standards evolved in healthcare, e.g. HL7, ICD, Direct, mHealth
* HL7 has traditionally been an XML dump
* FHIR is an open-source modern API, originally driven by Grahame Grieve in Australia
* Electronic medical record companies (e.g., Epic w/ 30%+ share) beginning to offer these APIs
  * startups utilizing APIs such as https://1up.health/ 

???
Pretty sure that every major hospital in the Bay Area use Epic, while smaller EHR systems are used by smaller practices.

---
#### Questions
These slides are are available at https://gitlab.com/jcrben/healthcare-data-talk